import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import store from './store';
import Forecast from './components/Forecast';


class App extends React.Component {
  render() {
  return (
    <Provider store={store}>
            <Forecast />
    </Provider>
    );
  }
}

export default App;