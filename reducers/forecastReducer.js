let initialState = {
  'forecast': {},
  'weather': {},
  'city': {},
};

export const forecastReducer = (state = initialState, action) => {
switch (action.type) {
  case 'UPDATE_CITY':
        return {
        ...state,
        city: action.payload,
        };
    case 'UPDATE_FORECAST':
        return {
          ...state,
          forecast: action.value
        };
    case 'UPDATE_WEATHER':
        return {
          ...state,
          weather: action.payload
        };
    default :
        return state;
}
};

export default forecastReducer;
