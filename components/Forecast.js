import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ForecastForm from './ForecastForm';
import ForecastTitle from './ForecastTitle';
import { updateCity, updateWeatherData } from '../actions/forecast';
import { ImageBackground, StyleSheet, Text, View, KeyboardAvoidingView, Dimensions } from 'react-native';
import gradient from '../assets/gradient.jpg';
import { Icon } from 'react-native-elements';

class Forecast extends React.Component {
  
    constructor(props) {
        super(props);
    }

    render() {

      // Get date format to string
      let localeDate = this.props.localtime;
      let date = new Date( localeDate*1000 ).toDateString();

      // Avoid result bloc when typing on keyboard
      const keyboardVerticalOffset = Platform.OS === 'android' ? -190 : 0
    
        return(
            <ImageBackground source={gradient} style={styles.bckColor}>
            <KeyboardAvoidingView style={styles.forecast} behavior='position' keyboardVerticalOffset={keyboardVerticalOffset}>

                <ForecastTitle/>

                    <View style={ this.props.description ? styles.result : styles.none} >

                      <Text onChangeText={ (event) => this.props.updateCity(event.target.value) } style={styles.city} >{this.props.name}</Text>
                      <Text onChangeText={ (event) => this.props.updateCity(event.target.value) } style={styles.date} >{date}</Text>
                      
                      <View style={styles.observations}>
                        <Text style={styles.temperature} onChangeText={ (event) => this.props.updateWeatherData(event.target.value) } >{this.props.temperature ? this.props.temperature + ' °': ''}</Text>

                        <View style={styles.details}>
                            {/**<Icon name="air" color="white" size={30}></Icon>*/}
                            <Text style={styles.wind} onChangeText={ (event) => this.props.updateWeatherData(event.target.value) } >{this.props.wind ? this.props.wind + ' km/h': ''} </Text>
                            <Text style={styles.humid} onChangeText={ (event) => this.props.updateWeatherData(event.target.value) } >{this.props.humidity ? this.props.humidity + ' %' : ''}</Text>
                        </View>
                      </View>
                      <Text style={styles.description} onChangeText={ (event) => this.props.updateWeatherData(event.target.value) } >{this.props.description}</Text>


                    </View> 
                    
                  <ForecastForm style={styles.form}/>

              </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

// Map redux states from api data
const mapStateToProps = (state) => {
    return {
      city: state.city.name,
      localtime: state.city.localtime_epoch,
      name: state.city.name,
      temperature:state.weather.temperature,
      description:state.weather.weather_descriptions,
      icon:state.weather.weather_icons,
      wind:state.weather.wind_speed,
      humidity:state.weather.humidity,

    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators( {
        updateCity,
        updateWeatherData
    }, dispatch);
  }

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Forecast);

  // Style forecast view 
  const styles = StyleSheet.create({
    
    bckColor: {
      resizeMode: 'cover',
      color:'white',
      width: '100%', 
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      margin: 0,
      padding:0

    },
    forecast: {
      display: 'flex',
      justifyContent: 'center',
      textAlign:'center',
      margin: 0,
      padding: 0,
    },
    title: {
      display: 'flex',
      justifyContent: 'center',
    },

    // SEARCH RESULT
    city: {
      display: 'flex',
      textAlign: 'center',
      color: 'white',
      fontSize: 18,
      textTransform: 'uppercase',
      fontWeight: 'bold',
    },
    date: {
      textAlign: 'center',
      color: 'rgba(255, 255, 255, 0.44)',
      fontSize: 12,

    },
    description: {
      color: 'white',
      textAlign: 'center',
    },
    temperature: {
      color: 'white',
      fontWeight: 'bold',
      display: 'flex',
      textAlign: 'center',
      fontSize: 47,
      paddingBottom: 20

    },
    observations: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 35
    },
    details: {
      top: -10,
      paddingLeft: 10,
    },
    result: {
      backgroundColor: '#0f0f39',
      justifyContent: 'center',
      height: '62%',
      borderRadius: 10,
      margin: 25,
      top: 45,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 0.58,
      shadowRadius: 16.00,
    },
    wind: {
      color: 'rgba(255, 255, 255, 0.44)',
      fontSize: 15
    },
    humid: {
      color: 'rgba(255, 255, 255, 0.44)',
      fontSize: 15
    },
    none: {
      display: 'none'
    },
    form : {
      alignContent: 'center'
    }
  });