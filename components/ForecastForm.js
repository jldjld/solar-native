import React from 'react';
import { fetchForecast } from '../actions/forecast';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';

class ForecastForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            city : ''
        }
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    updateCity = (city) => {
        this.setState({ city });
    }


    deleteCity = () => {
        this.setState({ city: '' });
    }
       
    render() {
        return(
            <View style={styles.forecastForm}>
                <View style={styles.searchIcon} >
                    <Icon name="search" size={18} color="rgba(0, 0, 0, 0.3)"/>
                </View>
                    
                <TouchableOpacity
                style={styles.closeIconContainer}
                onPress={this.deleteCity}
                >
                    <Icon style={styles.closeIcon} name="close" size={18} color="#000"/>
                </TouchableOpacity>

                <TextInput 
                 style={styles.input}
                 value={this.state.city} 
                 onChangeText={this.updateCity} 
                 placeholder="Search a city" 
                 onSubmitEditing={this.fetchForecast.bind(this)}/>

            </View>
        );
    }
}

// Map states to allow access in components
const mapStateToProps = (state) => {
    return {
    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators( {
        fetchForecast
    }, dispatch);
  }

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(ForecastForm);


  const styles = StyleSheet.create({
    forecastForm: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 0,
        padding: 0
    },
    input: {
        backgroundColor: 'white',
        opacity: 0.95,
        borderRadius: 50,
        width: 200,
        padding: 5,
        paddingLeft: 35,
        marginBottom: 10,
        position: 'relative'

    },
    closeIconContainer: {
        left: 225,
        bottom: -10,
        backgroundColor: 'rgba(255, 255, 255, 0.01)',
        padding: 30,
        elevation: 200,
        position: 'absolute',
        justifyContent: 'center'
    },
    closeIcon: {
        margin: 'auto'
    },
    searchIcon: {
        left: 30,
        top: -4,
        elevation: 90,
    
    }
    
  });
