import React from 'react';
import { StyleSheet, Text } from 'react-native';

class ForecastTitle extends React.Component {

    render() {
        return(
              <Text style={styles.title}>Forecast Weather</Text>
        );
    }
}

export default ForecastTitle;

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        textTransform: 'uppercase',
        paddingBottom: 30
    },
  });
  