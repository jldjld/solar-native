import { REACT_APP_API_KEY } from "@env";
import axios from "axios";

export const updateForecast = (forecast) => {
    return {
        type:'UPDATE_FORECAST',
        forecast
    }
  };

export const updateCity = (city) => {
    return {
        type:'UPDATE_CITY',
        payload:city
    }
};

export const updateWeatherData = (weather) => {
  return {
      type:'UPDATE_WEATHER',
      payload:weather
  }
};


export const fetchForecast = city => {

    return async dispatch => {
        axios.get('http://api.weatherstack.com/current?access_key=' + REACT_APP_API_KEY + city )
        .then(({data}) => {
        dispatch(updateForecast(data))
        dispatch(updateCity(data.location))
        dispatch(updateWeatherData(data.current))
        })
        .catch(function(error) {
            throw error;
        });
    }
}
