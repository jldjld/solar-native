import { applyMiddleware, createStore } from "redux";
import forecastReducer from './reducers/forecastReducer';
import thunk from 'redux-thunk';
import { composeWithDevTools } from "redux-devtools-extension";

const composeEnhancers = composeWithDevTools({
  });

const middleWare = [
  thunk,
];

let store = createStore(forecastReducer, composeEnhancers(applyMiddleware(...middleWare)));
export default store;
